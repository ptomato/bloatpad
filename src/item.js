// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2021 Philip Chimento

import GObject from "gi://GObject";
import Gtk from "gi://Gtk";

export const NotesListItem = GObject.registerClass(
  {
    GTypeName: "BloatpadNotesListItem",

    Properties: {
      title: GObject.ParamSpec.string(
        "title",
        "Title",
        "Note title",
        GObject.ParamFlags.READWRITE,
        ""
      ),
      excerpt: GObject.ParamSpec.string(
        "excerpt",
        "Excerpt",
        "Note excerpt",
        GObject.ParamFlags.READWRITE,
        ""
      ),
    },

    Template: "resource:///name/ptomato/Bloatpad/item.ui",
    InternalChildren: ["excerpt", "title"],
  },
  class NotesListItem extends Gtk.Grid {
    get title() {
      return this._title.label;
    }

    set title(value) {
      if (this._title.label === value) return;
      this._title.label = value;
      this.notify("title");
    }

    get excerpt() {
      return this._excerpt.label;
    }

    set excerpt(value) {
      if (this._excerpt.label === value) return;
      this._excerpt.label = value;
      this.notify("excerpt");
    }

    bind(note) {
      const f = GObject.BindingFlags.SYNC_CREATE;
      this._titleBinding = note.bind_property("title", this, "title", f);
      this._excerptBinding = note.bind_property("excerpt", this, "excerpt", f);
    }

    cleanup() {
      if (this._titleBinding) this._titleBinding.unbind();
      if (this._excerptBinding) this._excerptBinding.unbind();
    }
  }
);
