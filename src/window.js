// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2021 Philip Chimento

import Gdk from "gi://Gtk";
import Gio from "gi://Gio";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk";

import { NotesListItem } from "./item.js";

export const BloatpadWindow = GObject.registerClass(
  {
    GTypeName: "BloatpadWindow",
    Template: "resource:///name/ptomato/Bloatpad/window.ui",
    InternalChildren: [
      "backButton",
      "centerStack",
      "headerBar",
      "menuButton",
      "noteEditor",
      "notesList",
      "noteTitle",
    ],
  },
  class BloatpadWindow extends Gtk.ApplicationWindow {
    #current = null; // Note currently being edited

    #deleteAction = new Gio.SimpleAction({
      name: "delete",
      enabled: false,
    });

    #factory = new Gtk.SignalListItemFactory();

    #factoryConnections = [];

    constructor(application, props = {}) {
      super({
        application,
        ...props,
      });

      this.#deleteAction.connect("activate", this.#onDelete.bind(this));
      this.add_action(this.#deleteAction);

      this._menuButton.set_menu_model(this.application.get_menu_by_id("menu"));

      const { notes } = this.application;
      notes.connect("items-changed", this.#onNotesItemsChanged.bind(this));
      this._notesList.model = new Gtk.SingleSelection({ model: notes });

      this.#factoryConnections = [
        this.#factory.connect("setup", (_, listitem) => {
          listitem.child = new NotesListItem();
        }),
        this.#factory.connect("bind", (_, { item, child }) => child.bind(item)),
        this.#factory.connect("unbind", (_, { child }) => child.cleanup()),
      ];
      this._notesList.factory = this.#factory;
    }

    // Note currently being edited
    get current() {
      return this.#current;
    }

    set current(note) {
      if (this.#current === note) return;
      this.#current = note;
      if (note) {
        this._noteEditor.buffer = note.textBuffer;
        this._noteTitle.buffer = note.titleBuffer;
      }
    }

    vfunc_close_request() {
      this.#factoryConnections.forEach((id) => this.#factory.disconnect(id));
      return Gdk.EVENT_PROPAGATE;
    }

    showNewNote(note) {
      this.showEditor(note);
      this._noteTitle.grab_focus();
    }

    showEditor(note) {
      this.current = note;
      this._centerStack.transitionType = Gtk.StackTransitionType.OVER_UP_DOWN;
      this._centerStack.visibleChildName = "note-editor";
    }

    _onBackButtonClicked() {
      this.#showNotes();
    }

    #onDelete() {
      if (this._centerStack.visibleChildName === "note-editor")
        this.#showNotes();
      const { notes } = this.application;
      notes.remove(this._notesList.model.selected);
    }

    #onNotesItemsChanged(notes) {
      const empty = notes.get_n_items() === 0;
      this.#deleteAction.enabled = !empty;
      if (!["empty", "notes-list"].includes(this._centerStack.visibleChildName))
        return;
      this._centerStack.transitionType = Gtk.StackTransitionType.CROSSFADE;
      if (empty) this._centerStack.visibleChildName = "empty";
      else this._centerStack.visibleChildName = "notes-list";
    }

    _onNotesListActivate(listview, position) {
      const note = this.application.notes.get_item(position);
      this.showEditor(note);
      this._noteEditor.grab_focus();
    }

    _onNoteTitleActivate() {
      this._noteEditor.grab_focus();
    }

    _onStackPageChanged() {
      this._backButton.visible =
        this._centerStack.visibleChildName === "note-editor";
    }

    #showNotes() {
      if (this.application.notes.get_n_items() === 0) {
        this._centerStack.visibleChildName = "empty";
      } else {
        this._centerStack.visibleChildName = "notes-list";
        this.#updateSelectedNote();
      }
      this.current = null;
    }

    #updateSelectedNote() {
      const { notes } = this.application;
      for (let pos = 0; pos < notes.get_n_items(); pos++) {
        if (notes.get_item(pos) === this._current) {
          this._notesList.model.selected = pos;
          break;
        }
      }
    }
  }
);
