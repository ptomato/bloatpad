// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2021 Philip Chimento

// ignore unused exports main

import Gdk from "gi://Gdk?version=4.0";
import Gio from "gi://Gio";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

import { Note } from "./note.js";
import { BloatpadWindow } from "./window.js";

const Bloatpad = GObject.registerClass(
  class Bloatpad extends Gtk.Application {
    #notes = new Gio.ListStore({ itemType: Note });

    constructor(props = {}) {
      super({
        applicationId: "name.ptomato.Bloatpad",
        flags: Gio.ApplicationFlags.FLAGS_NONE,
        ...props,
      });
    }

    get notes() {
      return this.#notes;
    }

    vfunc_startup() {
      super.vfunc_startup();

      [
        {
          name: "new",
          activate: () => this.#newNote(),
        },
        {
          name: "about",
          activate: () => this.#about(),
        },
        {
          name: "quit",
          activate: () => this.quit(),
        },
      ].forEach(({ activate = null, ...props }) => {
        const action = new Gio.SimpleAction(props);
        if (activate) action.connect("activate", activate);
        this.add_action(action);
      });

      this.set_accels_for_action("app.new", ["<primary>N"]);
      this.set_accels_for_action("app.quit", ["<primary>Q"]);

      const provider = new Gtk.CssProvider();
      provider.load_from_file(
        Gio.File.new_for_uri(`resource://name/ptomato/Bloatpad/application.css`)
      );
      Gtk.StyleContext.add_provider_for_display(
        Gdk.Display.get_default(),
        provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
      );
    }

    vfunc_activate() {
      let { activeWindow } = this;
      if (!activeWindow) activeWindow = new BloatpadWindow(this);
      activeWindow.present();
    }

    #newNote() {
      const note = new Note();
      if (this.activeWindow) this.activeWindow.showNewNote(note);
      this.#notes.append(note);
    }

    #about() {
      new Gtk.AboutDialog({
        authors: ["Philip Chimento <philip.chimento@gmail.com>"],
        translatorCredits: _("translator-credits"),
        programName: _("Bloatpad"),
        comments: _("Unnecessary note-taking application"),
        copyright: "Copyright 2021–2022 Philip Chimento",
        licenseType: Gtk.License.MIT_X11,
        logoIconName: "accessories-text-editor",
        version: pkg.version,
        wrap_license: true,
        modal: true,
        transient_for: this.activeWindow,
      }).present();
    }
  }
);

export function main(argv) {
  return new Bloatpad().run(argv);
}
