// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2021 Philip Chimento

import GObject from "gi://GObject";
import Gtk from "gi://Gtk";

const RWCO = GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY;
const RO = GObject.ParamFlags.READABLE;

export const Note = GObject.registerClass(
  {
    Properties: {
      titleBuffer: GObject.ParamSpec.object(
        "title-buffer",
        "",
        "",
        RWCO,
        Gtk.EntryBuffer
      ),
      textBuffer: GObject.ParamSpec.object(
        "text-buffer",
        "",
        "",
        RWCO,
        Gtk.TextBuffer
      ),
      title: GObject.ParamSpec.string("title", "", "", RO, ""),
      excerpt: GObject.ParamSpec.string("excerpt", "", "", RO, ""),
      text: GObject.ParamSpec.string("text", "", "", RO, ""),
    },
  },
  class Note extends GObject.Object {
    #excerpt = null;

    constructor(props = {}) {
      const titleBuffer = new Gtk.EntryBuffer({
        text: _("Give your note a title…"),
      });
      const textBuffer = new Gtk.TextBuffer();
      super({
        titleBuffer,
        textBuffer,
        ...props,
      });

      titleBuffer.connect(
        "notify::text",
        this.#onTitleBufferChanged.bind(this)
      );
      textBuffer.connect("changed", this.#onTextBufferChanged.bind(this));
    }

    get title() {
      return this.titleBuffer.text;
    }

    // Excerpt to display in list
    get excerpt() {
      if (this.#excerpt) return this.#excerpt;

      const { text } = this;
      const newline = text.indexOf("\n");
      this.#excerpt = newline === -1 ? text : text.slice(0, newline);
      return this.#excerpt;
    }

    // Full text of note
    get text() {
      return this.textBuffer.text;
    }

    #onTitleBufferChanged() {
      this.notify("title");
    }

    #onTextBufferChanged() {
      this.notify("text");
      this.#excerpt = null;
      this.notify("excerpt");
    }
  }
);
